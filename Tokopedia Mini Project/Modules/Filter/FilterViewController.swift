//
//  FilterViewController.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit
import RangeSeekSlider

enum ShopType {
    case gold
    case official
}

protocol ShopFilterDelegate {
    func returnShopFilter(_ shopFilter: [ShopType])
}

class FilterViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var priceSlider: RangeSeekSlider!
    @IBOutlet weak var minPriceLabel: UILabel!
    @IBOutlet weak var maxPriceLabel: UILabel!
    @IBOutlet weak var shopColView: UICollectionView!
    @IBOutlet weak var wholesaleSwitch: UISwitch!
    @IBOutlet weak var shopContainerView: UIView!
    @IBOutlet weak var shopChevron: UIImageView!
    
    var param : ProductParameter?
    var minPrice = 0
    var maxPrice = 0
    var shopFilter: [ShopType]?
    var filterDelegate: FilterDelegate?
    
    @IBAction func applyTapped(_ sender: Any) {
        guard var param = param else { return }
        
        param.minPrice = minPrice
        param.maxPrice = maxPrice
        param.isWholesale = wholesaleSwitch.isOn
        param.isGold = ((shopFilter?.first(where: {$0 == ShopType.gold})) != nil)
        param.isOfficial = ((shopFilter?.first(where: {$0 == ShopType.official})) != nil)
        
        filterDelegate?.returnParam(param)
        navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        updateView()
    }
    
    override func setupView() {
        title = "Filter"
        view.backgroundColor = UIColor.Material.grey100
        priceSlider.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(shopTapped(_:)))
        shopContainerView.addGestureRecognizer(tap)
        shopChevron.tintColor = UIColor.Material.green500
        FilterShopCollectionViewCell.register(for: shopColView)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(resetTapped))
    }
    
    override func updateView() {
        guard let param = param else { return }
        
        minPrice = param.minPrice
        maxPrice = param.maxPrice
        priceSlider.selectedMinValue = minPrice.cgFloat
        priceSlider.selectedMaxValue = maxPrice.cgFloat
        priceSlider.layoutSubviews()
        minPriceLabel.text = "Rp\(minPrice.formatNumber(groupingSeparator: "."))"
        maxPriceLabel.text = "Rp\(maxPrice.formatNumber(groupingSeparator: "."))"
        
        wholesaleSwitch.isOn = param.isWholesale
        
        if shopFilter == nil {
            shopFilter = []
            if param.isGold {
                shopFilter?.append(.gold)
            }
            if param.isOfficial {
                shopFilter?.append(.official)
            }
            shopColView.reloadData()
        }
    }
    
    @objc func shopTapped(_ sender: Any) {
        let vc = ShopFilterViewController.loadFromNib()
        vc.shopFilter = shopFilter
        vc.shopFilterDelegate = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func resetTapped() {
        param = ProductParameter()
        shopFilter = nil
        updateView()
    }
    
}

extension FilterViewController: ShopFilterDelegate {
    
    func returnShopFilter(_ shopFilter: [ShopType]) {
        self.shopFilter = shopFilter
        shopColView.reloadData()
    }
    
}

extension FilterViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shopFilter?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FilterShopCollectionViewCell.self), for: indexPath) as? FilterShopCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.shopType = shopFilter?[safe: indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? FilterShopCollectionViewCell, let shopType = cell.shopType else { return }
        shopFilter?.removeAll(shopType)
        collectionView.reloadData()
    }
    
}

extension FilterViewController: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        minPrice = minValue.int
        maxPrice = maxValue.int
        minPriceLabel.text = "Rp\(minPrice.formatNumber(groupingSeparator: "."))"
        maxPriceLabel.text = "Rp\(maxPrice.formatNumber(groupingSeparator: "."))"
    }
    
}
