//
//  ShopFilterViewController.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/3/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit

class ShopFilterViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let rows: [ShopType] = [.gold, .official]
    var shopFilterDelegate: ShopFilterDelegate?
    var shopFilter: [ShopType]?
    
    @IBAction func applyTapped(_ sender: Any) {
        guard let indexPaths = tableView.indexPathsForSelectedRows else { return }
        var newShopFilter: [ShopType] = []
        for indexPath in indexPaths {
            guard let cell = tableView.cellForRow(at: indexPath) as? ShopFilterTableViewCell, let shopType = cell.shopType else { return }
            newShopFilter.append(shopType)
        }
        
        shopFilterDelegate?.returnShopFilter(newShopFilter)
        navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }
    
    override func setupView() {
        title = "Shop Type"
        ShopFilterTableViewCell.register(for: tableView)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(resetTapped))
    }
    
    override func updateView() {
        tableView.reloadData()
    }
    
    @objc func resetTapped() {
        updateView()
    }

}

extension ShopFilterViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ShopFilterTableViewCell.self), for: indexPath) as? ShopFilterTableViewCell else {
            return UITableViewCell()
        }
        
        cell.shopType = rows[safe: indexPath.row]
        if shopFilter?.first(where: {$0 == cell.shopType}) != nil {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        
        return cell
    }
    
}
