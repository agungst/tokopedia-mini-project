//
//  SearchViewController.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit
import PKHUD

protocol FilterDelegate {
    func returnParam(_ param: ProductParameter)
}

class SearchViewController: UIViewController {

    @IBOutlet weak var colView: UICollectionView!
    
    let productsReq = ProductsRequest()
    let rowsPerReq = 10
    var param = ProductParameter()
    var productList: ProductList?
    var isLoading = false
    
    @IBAction func filterTapped(_ sender: Any) {
        let vc = FilterViewController.loadFromNib()
        vc.param = param
        vc.filterDelegate = self
        navigationController?.pushViewController(vc, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        updateView()
    }
    
    override func updateView() {
        productRequest()
    }
    
    override func setupView() {
        title = "Search"
        colView.backgroundColor = UIColor.Material.grey100
        SearchProductCollectionViewCell.register(for: colView)
    }

    func productRequest() {
        productsReq.prepare(param: param, offset: productList?.arrProduct.count ?? 0, limit: rowsPerReq, requestHandler: self)
        productsReq.start()
        HUD.show(.progress)
        isLoading = true
    }

}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList?.arrProduct.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SearchProductCollectionViewCell.self), for: indexPath) as? SearchProductCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.product = productList?.arrProduct[safe: indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width - 48) / 2
        let height = width * 1.45
        return CGSize(width: width, height: height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.scrolledToBottom, !isLoading, productList?.arrProduct.count ?? 0 < productList?.totalData ?? 0 {
            productRequest()
        }
    }
    
}

extension SearchViewController: FilterDelegate {
    
    func returnParam(_ param: ProductParameter) {
        self.param = param
        productList = nil
        updateView()
        colView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
    }
    
}

extension SearchViewController: ApiDelegate {
    
    func success(_ object: Any) {
        HUD.hide()
        isLoading = false
        guard let obj = object as? ProductList else { return }
        
        if productList == nil {
            productList = obj
        } else {
            productList?.arrProduct.append(contentsOf: obj.arrProduct)
        }
        
        colView.reloadData()
    }
    
    func failed(_ message: String) {
        HUD.hide()
        isLoading = false
        showAlert(title: nil, message: message)
    }
    
}
