//
//  Endpoints.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import Foundation

private protocol Endpoint {
    var url: String { get }
}

private struct Api {
    static let baseUrl = "https://ace.tokopedia.com"
}


enum Endpoints {
    
    enum App: Endpoint {
        
        case products
        
        public var url: String {
            switch self {
            case .products: return "\(Api.baseUrl)/search/v2.5/product"
            }
        }
        
    }
    
}
