//
//  ProductsRequest.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ProductsRequest: ApiInterface {
    
    var method: HTTPMethod?
    
    var url: URLConvertible
    
    var headers: HTTPHeaders?
    
    var parameters: Parameters?
    
    var encoding: ParameterEncoding?
    
    var requestHandler: ApiDelegate?
    
    
    // MARK: Inits
    required init() {
        method = .get
        url = Endpoints.App.products.url
    }
    
    func start() {
        ApiHelper.apiRequest(api: self)
    }
    
    func prepare(param: ProductParameter, offset: Int, limit: Int, requestHandler: ApiDelegate) {
        self.requestHandler = requestHandler
                
        parameters = [
            "q" : param.query,
            "pmin" : param.minPrice,
            "pmax" : param.maxPrice,
            "wholesale" : param.isWholesale ? "true" : "false",
            "official" : param.isOfficial ? "true" : "false",
            "fshop" : param.isGold ? "2" : "",
            "start" : offset,
            "rows" : limit,
        ]
        
    }
    
    func success(_ value: JSON) {
        if value["status"]["error_code"].intValue == 0 {
            createObject(value)
        } else {
            failed(nil)
        }
    }
    
    func failed(_ value: JSON?) {
        requestHandler?.failed(value?["status"]["message"].string ?? "Unknown error occured")
    }
    
    func createObject(_ value: Any) {
        guard let json = value as? JSON else { return }
        if let dict = json.dictionaryObject, let response = ProductList(JSON: dict) {
            requestHandler?.success(response)
        } else {
            failed(nil)
        }
    }
    
}
