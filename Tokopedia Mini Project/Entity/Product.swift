//
//  Product.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import Foundation
import ObjectMapper

class Product: Mappable {
    
    var id = 0
    var name = ""
    var imageUri = ""
    var price = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        imageUri <- map["image_uri"]
        price <- map["price"]
    }
    
}

class ProductList: Mappable {
    
    var totalData = 0
    var arrProduct: [Product] = []
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        totalData <- map["header.total_data"]
        arrProduct <- map["data"]
    }
    
}
