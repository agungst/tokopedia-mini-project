//
//  Filter.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import Foundation

struct ProductParameter {
    
    var query = ""
    var minPrice = 100
    var maxPrice = 10000000
    var isWholesale = false
    var isOfficial = false
    var isGold = false
    
}
