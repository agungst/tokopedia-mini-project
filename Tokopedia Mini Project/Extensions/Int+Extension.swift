//
//  Int+Extension.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/3/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit

extension Int {
    
    func formatNumber(groupingSeparator: String?) -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = (groupingSeparator != nil) ? groupingSeparator! : ","
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self)) ?? "0"
    }
    
}
