//
//  UIScrollView+Extension.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    var scrolledToTop: Bool {
        let topEdge = 0 - contentInset.top
        return contentOffset.y <= topEdge
    }
    
    var scrolledToBottom: Bool {
        let bottomEdge = contentSize.height + contentInset.bottom - bounds.height
        return contentOffset.y >= bottomEdge
    }
    
}
