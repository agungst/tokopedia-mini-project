//
//  UICollectionViewCell+Extension.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    static func register(for collectionView: UICollectionView)  {
        let cellName = String(describing: self)
        let cellIdentifier = cellName
        let cellNib = UINib(nibName: String(describing: self), bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
}
