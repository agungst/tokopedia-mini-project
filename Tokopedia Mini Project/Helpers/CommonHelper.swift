//
//  CommonHelper.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit
import Alamofire

class CommonHelper {
    
    // MARK: - Common Functions
    static let shared = CommonHelper()
    private init() {} //This prevents others from using the default '()' initializer for this class.
    
    func isConnectedToInternet() -> Bool {
        let networkReachabilityManager = NetworkReachabilityManager()
        return networkReachabilityManager?.isReachable ?? false
    }
    
}
