//
//  SearchProductCollectionViewCell.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/2/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit
import Kingfisher
import SwifterSwift

class SearchProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var radiusView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var product: Product? {
        didSet {
            guard let product = product else { return }
            
            if let url = URL(string: product.imageUri) {
                imageView.kf.indicatorType = .activity
                imageView.kf.setImage(with: url)
            }
            
            nameLabel.text = product.name
            priceLabel.text = product.price
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clipsToBounds = false
        radiusView.cornerRadius = 8
        shadowView.addShadow(ofColor: .black, radius: 4, offset: CGSize.zero, opacity: 0.15)
    }

}
