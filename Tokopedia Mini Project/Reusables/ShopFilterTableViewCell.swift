//
//  ShopFilterTableViewCell.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/3/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit
import M13Checkbox

class ShopFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var checkbox: M13Checkbox!
    @IBOutlet weak var captionLabel: UILabel!
    
    var shopType: ShopType? {
        didSet {
            guard let shopType = shopType else { return }
            switch shopType {
            case .gold:
                captionLabel.text = "Gold Merchant"
                break
            case .official:
                captionLabel.text = "Official Store"
                break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkbox.boxType = .square
        checkbox.tintColor = UIColor.Material.green500
        checkbox.boxLineWidth = 2
        checkbox.checkmarkLineWidth = 2
        checkbox.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            checkbox.setCheckState(.checked, animated: false)
        } else {
            checkbox.setCheckState(.unchecked, animated: false)
        }
    }
    
}
