//
//  FilterShopCollectionViewCell.swift
//  Tokopedia Mini Project
//
//  Created by Agung Setiawan on 12/3/19.
//  Copyright © 2019 Agung. All rights reserved.
//

import UIKit

class FilterShopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    
    var shopType: ShopType? {
        didSet {
            guard let shopType = shopType else { return }
            switch shopType {
            case .gold:
                captionLabel.text = "Gold Merchant"
                break
            case .official:
                captionLabel.text = "Official Store"
                break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        borderView.cornerRadius = 20
        borderView.borderWidth = 1
        borderView.borderColor = UIColor.Material.grey300
        imageView.cornerRadius = 20
        imageView.borderWidth = 1
        imageView.borderColor = UIColor.Material.grey300
        imageView.tintColor = UIColor.Material.grey300
        imageView.backgroundColor = UIColor.Material.grey50
    }

}
